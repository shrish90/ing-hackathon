package com.ing.DAO.DAOImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ing.DAO.TestDAO;

@Repository
public class TestDAOImpl implements TestDAO{
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String getTodaysRateQuery = "INSERT INTO testtable (testData) VALUES (?)";
	
	@Override
	public String persistData(String data) {
		jdbcTemplate.update(getTodaysRateQuery, data);
		return "success";
	}

}
