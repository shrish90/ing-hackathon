package com.ing.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.DAO.TestDAO;
import com.ing.service.TestService;

@Service
public class TestServiceImpl implements TestService {
	
	@Autowired
	TestDAO testDao;
	
	@Override
	public String saveThisData(String data) {
		return testDao.persistData(data);
	}

}
