package com.ing.controllers;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ing.service.TestService;


/**
* <h1>Test Controller </h1>
* This is just an example of simple test service
*
* @author  Shrish Tiwari
* @version 1.0
* @since   2019-10-15
*/

@RestController
@RequestMapping("/test")
@CrossOrigin("*")
public class TestController {

@Autowired
TestService testService;

private static final Logger logger = LoggerFactory.getLogger(TestController.class);

@RequestMapping("/testIt")
public ResponseEntity<Map<String,String>> testController(){
	Map<String,String> result = new HashMap<>();
	logger.info("****RUNNING*****");
	result.put("root", "working fine");
	return new ResponseEntity<>(result, HttpStatus.OK);
}

/**
 * This method is used to save data into database with data passed in pathvariable
 * @param data This the the data to be saved in database
 * @return ResponseEntity return a success or failure message of data persistance.
 */
@RequestMapping(value="save/{data}")
public ResponseEntity<Map<String,String>> saveDataController(@PathVariable("data") String data){
	Map<String,String> result = new HashMap<>();
	testService.saveThisData(data);
	result.put("root", "saved");
	return new ResponseEntity<>(result, HttpStatus.CREATED);
}
}
